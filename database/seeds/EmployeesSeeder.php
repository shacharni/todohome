<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
                       'name' => 'roni',
                           'email' => 'roni111@jack.com',
                           'password'=>Hash::make('12345678'),
                            'created_at' => date('Y-m-d G:i:s'),
                        'role'=>'employee',
                            ],
                            [
                                'name' => 'roni2',
                                'email' => 'roni2111@john.com',
                                'password'=>Hash::make('12345678'),
                                'created_at' => date('Y-m-d G:i:s'),
                                'role'=>'employee',
                            ],
                            [
                                'name' => 'soosh',
                                'email' => 's@john.com',
                                'password'=>Hash::make('12345678'),
                                'created_at' => date('Y-m-d G:i:s'),
                                'role'=>'manager',
                            ],
                            [
                                'name' => 'shoos2',
                                'email' => 's1@john.com',
                                'password'=>Hash::make('12345678'),
                                'created_at' => date('Y-m-d G:i:s'),
                                'role'=>'manager',
                            ],
                           
                     ] );
            }
    }

