<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
             'title' => 'The Notebook',
            'author' => 'Nicholas Sparks',
            'user_id'=>'1',
            'status'=>'0',
        ],
        [ 'title' =>'The Longest Ride',
        'author' =>'Nicholas Sparks',
        'user_id'=>'1',
        'status'=>'0',
    ],
    [ 'title' =>'The Choice',
    'author' =>'Nicholas Sparks',
    'user_id'=>'1',
    'status'=>'0',
],
[ 'title' =>'Message in a Bottle',
'author' =>'Nicholas Sparks',
'user_id'=>'2',
'status'=>'0',
],
[ 'title' =>'The Lucky One',
'author' =>'Nicholas Sparks',
'user_id'=>'2',
'status'=>'0',
],
        ]);
    }
}
