<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('users')->insert([
                [
                 'name' => 'shachar',
                 'email'=>'shachar@gmail.com',
                 'password'=>'123123',
                 'created_at'=>date('Y-m-d G:i:s'),
                
            ],
            [
                'name' => 'hadar',
                'email'=>'hadar@gmail.com',
                'password'=>'123456',
               'created_at'=>date('Y-m-d G:i:s'),
           ],
       
            ]);
        }
    }
}
