@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
    <h1> Book list</h1>
        <table>
            <tr>
                
                <th>title</th>
                <th>author</th>
            </tr>
            @foreach($books as $book)
            <tr >
            
                <td>  @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif
       @can('manager')
       <a href="{{route('books.edit',$book->id)}}">
@endcan

         {{$book->title}}</a></td>
                <td>
                @can('manager')
                <a href="{{route('books.edit',$book->id)}}">
                @endcan  {{$book->author}}</a></td>
               
            </tr>
            @endforeach
        </table>
        @can('manager')
        <a href="{{route('books.create')}}">Create a new book</a>
        @endcan
        <script>
      $(document).ready(function(){
          $(":checkbox").click(function(event){
              console.log(event.target.id)
               $.ajax({
                   url: "{{url('books')}}"+'/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:"{{csrf_token()}}"}),
                   processData:false,
                  success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  
    </body>
</html>
@endsection