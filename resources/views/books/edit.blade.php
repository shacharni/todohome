@extends('layouts.app')

@section('content')
<h1>Update a Book</h1>
<form method='post' action="{{action('BookController@update',$book->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
        <label for ="title"> Book to update</label>
        <input type="text" class= "form-control" name="title" value="{{$book->title}}">
    </div>
    <div >
       
        <input type="text" class= "form-control" name="author" value="{{$book->author}}">
    </div>
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>
<form method='post' action="{{action('BookController@update',$book->id)}}">
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="delete">
    </div>
</form>
@endsection